import 'package:flutter/material.dart';

class ItemFromGridDetails extends StatefulWidget {
  final prod_name;

  final prod_pricture;
  final prod_old_price;
  final prod_price;

  @override
  _ItemFromGridDetailsState createState() => _ItemFromGridDetailsState();

  ItemFromGridDetails(
      this.prod_name, this.prod_pricture, this.prod_old_price, this.prod_price);
}

class _ItemFromGridDetailsState extends State<ItemFromGridDetails> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.yellowAccent,
        title: new Text("Apparel App"),
        centerTitle: true,
        elevation: 10.0,
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {}),
          new IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {})
        ],
      ),
      body: new ListView(
        children: <Widget>[
          //image carousel begins here

          Container(
            height: 180,
            child: GridTile(
              child: Container(
                  color: Colors.white,
                  child: Image.asset(widget.prod_pricture)),
              footer: new Container(
                color: Colors.white70,
                child: ListTile(
                  leading: new Text(
                    widget.prod_name,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.00),
                  ),
                  title: new Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("\$${widget.prod_old_price}",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w800,
                              decoration
                                  :TextDecoration.lineThrough),
                        ),


                      ),
                      Expanded(
                        child: new Text("\$${widget.prod_price}",
                          style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.w800,
                          ),),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  onPressed: () {


                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Size"),
                          content: new Text("Choose Size"),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("Close"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );

                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("Size"),
                      ),
                      Expanded(
                        child: new Icon(Icons.arrow_drop_down),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: MaterialButton(
                  onPressed: () {

                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Qty"),
                          content: new Text("Choose the  Qty"),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("Close"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("Qty"),
                      ),
                      Expanded(
                        child: new Icon(Icons.arrow_drop_down),
                      )
                    ],
                  ),



                ),
              ),

              Expanded(
                child: MaterialButton(
                  onPressed: () {

                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Brand"),
                          content: new Text("Choose the  Brand"),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("Close"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("Brand"),
                      ),
                      Expanded(
                        child: new Icon(Icons.arrow_drop_down),
                      )
                    ],
                  ),



                ),
              ),

            ],
          )
          ,
          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  onPressed: () {


                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  elevation: 2.0,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("Buy Now",textAlign: TextAlign.center,),
                      ),

                    ],
                  ),
                ),
              ),

              new IconButton(icon:Icon(Icons.add_shopping_cart,color: Colors.red,), onPressed: (){}),
              new IconButton(icon:Icon(Icons.favorite_border,color: Colors.red,), onPressed: (){})


            ],
          ),
          Divider(),
          new ListTile(
            title: new Text("Cloth Details"),
            subtitle: new Text("is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, b"),
          ),
          Divider(),
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Cloth Name",style: TextStyle(color: Colors.grey))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text(widget.prod_name,style: TextStyle(color: Colors.grey)))

            ],
          )
          ,
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Cloth Price",style: TextStyle(color: Colors.grey))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text("\$${widget.prod_price}",style: TextStyle(color: Colors.grey)))


            ],
          ),
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Quantity",style: TextStyle(color: Colors.grey))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text("10",style: TextStyle(color: Colors.grey)))


            ],
          )
        ],
      ),

    );
  }
}
