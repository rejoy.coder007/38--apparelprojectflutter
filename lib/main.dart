import 'package:flutter/material.dart';

import 'aa_HomeScreenApparel.dart';

/*

void main() {
  runApp(new MaterialApp(

      debugShowCheckedModeBanner: false,

    home:new HomeScreenApparel()

  ));
}






class SamplePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color:Colors.pink,
      child: Text("TestingShort",style: TextStyle(color: Colors.white),)


    );
  }
}
*/


import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:aa_flutter_app/aa_HomeScreenApparel.dart';

import 'package:aa_flutter_app/Login/AdminPage.dart';
import 'package:aa_flutter_app/Login/MemberPage.dart';
import 'package:aa_flutter_app/Config.dart';
 


import 'package:http/http.dart' as http;

/*

void main() {
  runApp(new MaterialApp(



      debugShowCheckedModeBanner: false,

    home:new TourHome()

  ));
}

*/



/*

class SamplePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color:Colors.pink,
      child: Text("TestingShort",style: TextStyle(color: Colors.white),)


    );
  }
}
*/


import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


void main() => runApp(new MyApp());

String username='';

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login PHP My Admin',
      home: new MyHomePage(),
      routes: <String,WidgetBuilder>{
        '/AdminPage': (BuildContext context)=> new AdminPage(username: username,),
        '/MemberPage': (BuildContext context)=> new MemberPage(username: username,),
        '/ApparelHome': (BuildContext context)=> new HomeScreenApparel(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController email=new TextEditingController();
  TextEditingController pass=new TextEditingController();






  int admin;
  int OK;

  String msg="hello";

  Future<String> _login() async
  {

    List data;

    print("ORGADDRESS :: " + Config.url_login);
    final response = await http.post(Uri.parse(Config.url_login), body: {

    //final response = await http.post(Uri.parse("http://192.168.0.101/aa_login"), body: {
      "email": email.text,
      "password": pass.text,
    }, headers: {"Accept": "application/json"});




    setState(()
    {
      var resBody = json.decode(response.body.toString());
      print("Value" + resBody.toString());

      data = resBody["results"];

      admin=data[0]["admin"];
      OK=data[0]["OK"];
      Config.Id=OK=data[0]["id"];

      msg=data[0]["msg"];

      if(data[0]['OK']==2){
        Navigator.pushReplacementNamed(context, '/ApparelHome');
      }

      print("Value" + resBody.toString());
    });






    return "Success!";
    /*

    var datauser = json.decode(response.body);

    if(datauser.length==0){
      setState(() {
        msg="Login Fail";
      });
    }else{
      if(datauser[0]['level']=='admin'){
        Navigator.pushReplacementNamed(context, '/AdminPage');
      }else if(datauser[0]['level']=='member'){
        Navigator.pushReplacementNamed(context, '/MemberPage');
      }

      setState(() {
        username= datauser[0]['username'];
      });

    }
*/
    // return datauser;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Apparel App"),),
      body: Container(

        decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage('images/cr_1.jpeg'), // Background Image
              fit: BoxFit.cover,
            )),
        child: Center(

          child: Column(

            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,

            children: <Widget>[

              Text("Username",style: TextStyle(fontSize: 18.0),),
              TextField(
                controller: email,
                decoration: InputDecoration(
                    hintText: 'Enter Email'
                ),
              ),
              Text("Password",style: TextStyle(fontSize: 18.0),),
              TextField(
                controller: pass,
                obscureText: true,
                decoration: InputDecoration(
                    hintText: 'Enter Password'
                ),
              ),

              RaisedButton(
                child: Text("Login"),
                onPressed: (){
                  _login();
                },
              ),

              Text(msg,style: TextStyle(fontSize: 20.0,color: Colors.red),)


            ],
          ),
        ),
      ),
    );
  }
}